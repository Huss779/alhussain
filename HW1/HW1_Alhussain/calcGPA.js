function Calculate() {
    var gradeEntries = document.getElementsByClassName("Grades");
    var creditEntries = document.getElementsByClassName("Credits");
    var weights = [];
    var credits = [];
    var i;
    for(i=0;i<gradeEntries.length;i++){
       weights[i] = gradeCalc(gradeEntries[i].value); 
       credits[i] = parseInt(creditEntries[i].value);  
    }

    var total = 0;
    for(i=0;i<credits.length;i++)
    total += credits[i];
    document.getElementById("totalCredits").innerHTML = total;
 // Compute GPA here 
    var gpa = 0.0;
    for(i=0;i<credits.length;i++)
        gpa += weights[i]*credits[i];
    gpa /= total;
 
 
  document.getElementById("gpa").innerHTML = gpa;
 // print GPA
}
function addRow(){
   var table = document.getElementById("tableId");
   if (table.rows.length >= 8){
       window.alert("You have reach the maximum number");
       return;
   }
   var table = document.getElementById("tableId");
   var row = table.insertRow(table.rows.length);
   var cell1 = row.insertCell(0);
   var cell2 = row.insertCell(1);
   var cell3 = row.insertCell(2);
   cell1.innerHTML = '<input type="text" >';
   cell2.innerHTML = '<input type="text" class="Credits">';
   cell3.innerHTML = '<input type="text" class="Grades">';
}

function deleteRow(){
   var table = document.getElementById("tableId");
   if (table.rows.length <= 3)
       window.alert("You have reach the minimum number");
       return;
   table.deleteRow(table.rows.length-1);
    
}
function gradeCalc(grade){
    weight = 0;
    if (grade == 'A+') weight = 4;
    else if (grade == 'A') weight = 3.75;
    else if (grade == 'B+') weight = 3.5;
    else if (grade == 'B') weight = 3;
    else if (grade == 'C+') weight = 2.5;
    else if (grade == 'C') weight = 2;
    else if (grade == 'D+') weight = 1.5;
    else if (grade == 'D') weight = 1;
    return weight;
}

